#Imagen base para docker
#FROM --platform=linux/amd64 python:3.12.0-slim-bookworm
FROM python:3.12.0-slim-bookworm

#Configurar variables entorno
ENV PIP_DISABLE_PIP_VERSION_CHECK 1
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

#Configurar directorio de trabajo
WORKDIR /code

#Instalar dependencia
COPY ./requirements.txt .
RUN pip install -r requirements.txt

#Copiar el proyecto
COPY . .

